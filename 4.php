<?php
//programa que permite su¡imular tiradas de dados, deja tirar 10 veces y muestra
//las 10 tiradas al final
//las tiradas se almacenan en un array bidimensional y un array que almacene las suma de las tiradas

//funcion que acepta dos argumentos pasados por referencia que simulen tiradas de dados y los almacena
//en un array llamado tiradas

function tirada(int $d1, int $d2): array
{
    $tirada = [];
    $tirada[0] = [$d1, $d2];
    return $tirada;
}

//inicializamos las variables
$d1 = 0;
$d2 = 0;
$sumaTiradas = [];
$tiradas = [];


//con un for generamos las tiradas y las sumamos
for ($c = 0; $c < 10; $c++) {
    $d1 = rand(1, 6);
    $d2 = rand(1, 6);
    $tiradas[$c] = tirada($d1, $d2);
    $sumaTiradas[$c] = $d1 + $d2;
}
var_dump($tiradas);
var_dump($sumaTiradas);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
<?php
//imprimimos los resultados con foreach
foreach($tiradas as $tirada){
    ?>
    <div>
        <img src="dados/<?= $tirada[0][0] ?>.svg">
        <img src="dados/<?= $tirada[0][1] ?>.svg">
    </div>
    <div>
        Total: <?= $sumaTiradas[0] ?>
    </div>
    <?php
}
?>
</body>

</html>