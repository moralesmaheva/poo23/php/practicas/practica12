<?php
// programa que simula 10 tiradas de dos dados y graba en un array la suma de cada tirada y
//muestra la tirada con puntuacion maxima

//inicializamos las variables
$dado1 = 0;
$dado2 = 0;
$tiradas = [];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <?php
    //generamos las tiradas y grabamos las tiradas sumadas e imprimimos el resultado
    for ($c = 0; $c < 10; $c++) {
        $dado1 = rand(1, 6);
        $dado2 = rand(1, 6);
        $tiradas[$c] = $dado1 + $dado2;

    ?>
        <div>
            <img src="dados/<?= $dado1 ?>.svg">
            <img src="dados/<?= $dado2 ?>.svg">
        </div>
        <div>
            Total: <?= $tiradas[$c] ?>
        </div>
    <?php
    }
    var_dump($tiradas);
    $max = max($tiradas);
    ?>
    <div>
        La mayor tirada es <?= $max ?>
    </div>
</body>

</html>