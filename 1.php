<?php
// programa que cada vez quye se ejecute muestre dos tiradas de dados e 
//indique el resultado final
//debe haber dos variables para colocar las anchuras y altura con css
//la caja que contiene los dos dados debe tener el doble de ancho que el de las fotos

//inicializamos las variables
$dado1 = 0;
$dado2 = 0;
$altodado = 0;
$anchodado = 0;
$altodiv = 0;
$anchodiv = 0;
$tirada = 0;

//generamos dos valores aleatorios
$dado1 = rand(1, 6);
$dado2 = rand(1, 6);

//calculamos las alturas y anchuras
$altodado = 100;
$anchodado = 100;
$altodiv = $altodado * 2;
$anchodiv = $anchodado * 2;
$tirada = $dado1 + $dado2;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
    <!-- <link rel="stylesheet" href="estilo1.css"> -->
</head>

<body>
    <div style="width:<?= $anchodiv ?>px; height:<?= $altodiv ?>px;">
        <img src="dados/<?= $dado1 ?>.svg" style="width:<?= $anchodado ?>px; height:<?= $altodado ?>px;">
        <img src="dados/<?= $dado2 ?>.svg" style="width:<?= $anchodado ?>px; height:<?= $altodado ?>px;">
    </div>
    <br>
    <div>
        Total: <?= $tirada ?>
    </div>
</body>

</html>