<?php
// programa que simula 10 tiradas de dados y los almacena en un array bidimensional asociativo


//inicializamos las variables
$dado1 = 0;
$dado2 = 0;
$tiradas = [];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3_1</title>
</head>

<body>
    <?php
    //generamos las tiradas y grabamo el resultado en el array bidimensional asociativo
    //imprimimos los dados
    for ($c = 0; $c < 10; $c++) {
        $dado1 = rand(1, 6);
        $dado2 = rand(1, 6);
        $tiradas[$c] = [
            'dado1' => $dado1,
            'dado2' => $dado2
        ]
    ?>
        <div>
            <img src="dados/<?= $dado1 ?>.svg">
            <img src="dados/<?= $dado2 ?>.svg">
        </div>
    <?php
    }
    var_dump($tiradas);
    ?>
</body>

</html>